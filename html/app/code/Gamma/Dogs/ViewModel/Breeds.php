<?php


namespace Gamma\Dogs\ViewModel;



use Gamma\Dogs\Api\BreedInfoInterfaceFactory;
use Gamma\Dogs\Api\BookInfoInterfaceFactory;
use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\Data\BookInterface;
use Gamma\Dogs\Api\Data\BreedInterfaceFactory;
use Gamma\Dogs\Api\Data\BookInterfaceFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Breeds implements ArgumentInterface
{

    protected $breedFactory;
    protected $connection;
    protected $request;
    protected $breedInfoFactory;
    protected $bookFactory;
    protected $bookInfoFactory;

    /**
     * Breeds constructor.
     * @param ConnectionInterface $connection
     * @param BreedInterfaceFactory $breedFactory
     * @param RequestInterface $request
     * @param BreedInfoInterfaceFactory $breedInfoFactory
     * @param BookInterfaceFactory $bookFactory
     * @param BookInfoInterfaceFactory $bookInfoFactory
     */
    public function __construct
    (
        ConnectionInterface $connection,
        BreedInterfaceFactory $breedFactory,
        RequestInterface $request,
        BreedInfoInterfaceFactory $breedInfoFactory,
        BookInterfaceFactory $bookFactory,
        BookInfoInterfaceFactory $bookInfoFactory
    )
    {
        $this->connection = $connection;
        $this->breedFactory = $breedFactory;
        $this->request = $request;
        $this->breedInfoFactory = $breedInfoFactory;
        $this->bookFactory = $bookFactory;
        $this->bookInfoFactory = $bookInfoFactory;
    }

    public function getBreed()
    {
        $requestedBreed = $this->request->getParam('breed');

        if ($requestedBreed) {
            $bookInfo = $this->bookInfoFactory->create();

            //$numberOfBooks = $bookInfo->getNumberOfBooks($requestedBreed);
            $numberOfBooks = 5;
            $books = array();
            for ($i = 0; $i < $numberOfBooks; $i++) {
                $book = $this->bookFactory->create();
                $book
                    ->setBookTitle($bookInfo->getBookTitle($requestedBreed, $i))
                    ->setCover($bookInfo->getCover($requestedBreed, $i))
                    ->setReviews($bookInfo->getReviews($requestedBreed, $i))
                    ->setRead($bookInfo->getLink($requestedBreed, $i))
                ;
                $books[$i] = $book;
            }

            $breedInfo = $this->breedInfoFactory->create();
            $breed = $this->breedFactory->create();
            $breed
                ->setImage($breedInfo->getImage($requestedBreed))
                ->setBreedName($requestedBreed)
                ->setBooks($books)
                ->setSubBreeds($breedInfo->getSubBreeds($requestedBreed))
            ;

            return $breed;
        }
        else {
            return $this->getAllBreeds();
        }
    }

    public function getAllBreeds()
    {
        $breedInfo = $this->breedInfoFactory->create();
        return $breedInfo->getAllBreeds();
    }

    public function argumentsExists()
    {
        return $this->request->getParams()? true : false;
    }
}