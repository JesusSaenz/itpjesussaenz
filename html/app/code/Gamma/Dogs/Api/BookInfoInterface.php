<?php


namespace Gamma\Dogs\Api;


interface BookInfoInterface
{
    public function getNumberOfBooks(string $searchTitle);

    public function getBookTitle(string $searchTitle, int $index);

    public function getReviews(string $searchTitle, int $index);

    public function getCover(string $searchTitle, int $index);

    public function getLink(string $searchTitle, int $index);

}