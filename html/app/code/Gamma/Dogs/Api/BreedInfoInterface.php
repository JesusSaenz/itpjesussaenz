<?php


namespace Gamma\Dogs\Api;
use Gamma\Dogs\Api\Data\BreedInterface;

interface BreedInfoInterface
{
    public function getBreed(string $breedName);

    public function getAllBreeds():array;

    public function getSubBreeds(string $mainBreed):array;

    public function getImage(string $breedName):string;
}