<?php


namespace Gamma\Dogs\Api\Data;


interface BookInterface
{
    const BOOK_TITLE = "bookTitle";
    const COVER = "cover";
    const REVIEWS = "reviews";
    const READ = "read";

    public function getBookTitle():string;

    public function setBookTitle(string $bookTitle):BookInterface;

    public function getCover():string;

    public function setCover(string $cover):BookInterface;

    public function getReviews():string;

    public function setReviews(string $reviews):BookInterface;

    public function getRead():string;

    public function setRead(string $url):BookInterface;

}