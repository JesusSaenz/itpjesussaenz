<?php


namespace Gamma\Dogs\Api\Data;


interface BreedInterface
{

    const BREED_NAME = 'breedName';
    const IMAGE = 'image';
    const SUB_BREEDS = 'subBreeds';
    const BOOKS = 'books';


    public function getBreedName():string;

    public function setBreedName(string $breedName):BreedInterface;

    public function getImage():string;

    public function setImage(string $url):BreedInterface;

    public function getSubBreeds():array;

    public function setSubBreeds(array $subBreeds):BreedInterface;

    public function getBooks():array;

    public function setBooks(array $books):BreedInterface;



}