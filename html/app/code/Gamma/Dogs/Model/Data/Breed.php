<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\BreedInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Breed extends AbstractSimpleObject implements BreedInterface
{

    public function getBreedName(): string
    {
        return $this->_get(self::BREED_NAME);
    }

    public function setBreedName(string $breedName): BreedInterface
    {
        return $this->setData(self::BREED_NAME, $breedName);
    }

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $url): BreedInterface
    {
        return $this->setData(self::IMAGE, $url);
    }

    public function getSubBreeds(): array
    {
        return $this->_get(self::SUB_BREEDS);
    }

    public function setSubBreeds(array $subBreeds): BreedInterface
    {
        return $this->setData(self::SUB_BREEDS, $subBreeds);
    }

    public function getBooks(): array
    {
        return $this->_get(self::BOOKS);
    }

    public function setBooks(array $books): BreedInterface
    {
        return $this->setData(self::BOOKS, $books);
    }
}