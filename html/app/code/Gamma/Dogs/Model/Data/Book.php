<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\BookInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Book extends AbstractSimpleObject implements BookInterface
{

    public function getBookTitle(): string
    {
        return $this->_get(self::BOOK_TITLE);
    }

    public function setBookTitle(string $bookTitle): BookInterface
    {
        return $this->setData(self::BOOK_TITLE, $bookTitle);
    }

    public function getCover(): string
    {
        return $this->_get(self::COVER);
    }

    public function setCover(string $cover): BookInterface
    {
        return $this->setData(self::COVER, $cover);
    }

    public function getReviews(): string
    {
        return $this->_get(self::REVIEWS);
    }

    public function setReviews(string $reviews): BookInterface
    {
        return $this->setData(self::REVIEWS, $reviews);
    }

    public function getRead(): string
    {
        return $this->_get(self::READ);
    }

    public function setRead(string $url): BookInterface
    {
        return $this->setData(self::READ, $url);
    }
}