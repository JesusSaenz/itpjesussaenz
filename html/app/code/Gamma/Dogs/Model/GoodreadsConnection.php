<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\GoodreadsConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;


class GoodreadsConnection implements GoodreadsConnectionInterface
{
    protected $baseUrl;

    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    protected $publicKey;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://www.goodreads.com',
        string $publicKey = "MbqP31t9vzd19eE2mW0hw"
    )
    {
        $this->jsonSerializer = $jsonSerializer;
        $this->baseUrl = $baseUrl;
        $this->httpClient = $httpClient;
        $this->publicKey = $publicKey;
    }

    public function get(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrl}/{$resourcePath}&key={$this->publicKey}";
        //$requestPath = "{$this->baseUrl}/{$this->searchArguments["bookUrl"]}?q=african%20dog&key={$this->publicKey}&search=title";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = $this->jsonSerializer->serialize($xml);

        return $this->jsonSerializer->unserialize($json);

    }



}