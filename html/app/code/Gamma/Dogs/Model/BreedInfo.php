<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\BreedInfoInterface;
use Gamma\Dogs\Api\ConnectionInterface;

class BreedInfo implements BreedInfoInterface
{

    protected $connection;

    /**
     * BreedInfo constructor.
     * @param ConnectionInterface $connection
     */
    public function __construct
    (
        ConnectionInterface $connection
    )
    {
        $this->connection = $connection;
    }

    public function getBreed(string $breedName)
    {
        $breed = $this->connection->get("breed/{$breedName}/images");
        return $breed["message"];
    }

    public function getImage(string $breedName): string
    {
        return $this->getBreed($breedName)[0];
    }

    public function getAllBreeds(): array
    {
        $allBreeds = $this->connection->get("breeds/list/all");
        return $allBreeds["message"];
    }

    public function getSubBreeds(string $mainBreed):array
    {
        $subBreeds = $this->connection->get("breed/{$mainBreed}/list");
        return $subBreeds["message"] != "Breed not found" ? $subBreeds["message"] : array();
    }
}