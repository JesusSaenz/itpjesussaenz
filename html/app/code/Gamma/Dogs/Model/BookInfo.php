<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\BookInfoInterface;
use Gamma\Dogs\Api\GoodreadsConnectionInterface;

class BookInfo implements BookInfoInterface
{

    protected $goodreadsConnection;

    /**
     * BookInfo constructor.
     * @param $goodreadsConnection
     */
    public function __construct
    (
        GoodreadsConnectionInterface $goodreadsConnection
    )
    {
        $this->goodreadsConnection = $goodreadsConnection;
    }

    public function getNumberOfBooks(string $searchTitle) {
        $bookData = $this->goodreadsConnection->get(
            "search/index.xml?q={$searchTitle}$20dog&search=title"
        );
        return count($bookData["search"]["results"]["work"]);
    }

    public function bestBook(array $bookData, string $field, int $workNumber = 0)
    {
        return $bookData["search"]["results"]["work"][$workNumber]["best_book"][$field] ?? "This field doesnt Exists";
    }

    public function getBookTitle(string $searchTitle, int $index = 0)
    {
        $bookData = $this->goodreadsConnection->get(
            "search/index.xml?q={$searchTitle}$20dog&search=title"
        );
        $bookTitle = $this->bestBook($bookData, "title", $index);
        return $bookTitle;
    }

    public function getReviews(string $searchTitle, int $index = 0)
    {
        $bookData = $this->goodreadsConnection->get(
            "search/index.xml?q={$searchTitle}$20dog&search=title"
        );
        $bookId = $this->bestBook($bookData, "id", $index);
        $reviewsData = $this->goodreadsConnection->get(
            "book/show.xml?id={$bookId}"
        );
        $reviews = $reviewsData["book"]["reviews_widget"];
        return $reviews;
    }

    public function getCover(string $searchTitle, int $index = 0)
    {
        $bookData = $this->goodreadsConnection->get(
            "search/index.xml?q={$searchTitle}%20dog&search=title"
        );
        $bookTitle = $this->bestBook($bookData, "image_url", $index);
        return $bookTitle;
    }

    public function getLink(string $searchTitle, int $index = 0)
    {
        $bookData = $this->goodreadsConnection->get(
            "search/index.xml?q={$searchTitle}$20dog&search=title"
        );
        $bookId = $this->bestBook($bookData, "id", $index);
        $linkData = $this->goodreadsConnection->get(
            "book/show.xml?id={$bookId}"
        );
        $link = $linkData["book"]["link"];
        return $link;
    }

    public function getMultipleBooks(string $searchTitle, int $numberOfBooks = 1)
    {
        $books = array();
        for ($i = 0; $i < $numberOfBooks; $i++) {
            $books[$i]["bookTitle"] =  $this->getBookTitle($searchTitle, $i);
            $books[$i]["Cover"] =  $this->getCover($searchTitle, $i);
            $books[$i]["reviews"] =  $this->getReviews($searchTitle, $i);
            $books[$i]["link"] =  $this->getLink($searchTitle, $i);
        }
        return $books;
    }

}