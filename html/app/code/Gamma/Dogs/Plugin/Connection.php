<?php


namespace Gamma\Dogs\Plugin;


class Connection
{
    protected $requestedData;

    public function aroundGet(\Gamma\Dogs\Model\Connection $connection, callable $proceed, string $resourcePath)
    {
        return $this->requestedData[$resourcePath] ?? $this->add($proceed, $resourcePath);
    }

    public function add($proceed, $resourcePath)
    {
        $this->requestedData[$resourcePath] = $proceed($resourcePath);
        return $this->requestedData[$resourcePath];
    }
}