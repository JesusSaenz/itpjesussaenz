<?php


namespace Gamma\Dogs\Plugin;


class GoodreadsConnection
{
    protected $requestedData;

    public function aroundGet(\Gamma\Dogs\Model\GoodreadsConnection $connection, callable $proceed, string $resourcePath)
    {
        return $this->requestedData[$resourcePath] ?? $this->add($proceed, $resourcePath);
    }

    public function add($proceed, $resourcePath)
    {
        $this->requestedData[$resourcePath] = $proceed($resourcePath);
        return $this->requestedData[$resourcePath];
    }
}